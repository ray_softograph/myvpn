package com.softograph.vpnapp.service

import android.net.VpnService
import android.os.ParcelFileDescriptor
import android.util.Log
import com.softograph.vpnapp.data.*
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.net.InetSocketAddress
import java.net.SocketException
import java.nio.ByteBuffer
import java.nio.channels.ByteChannel
import java.nio.channels.DatagramChannel
import java.nio.channels.SocketChannel
import java.nio.charset.StandardCharsets.US_ASCII
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class AppVpnConnection(
    private val service: VpnService,
    private val appConfig: AppVpnConfig,
    private val events: ConnectionEvents
) {
    companion object {
        /** Maximum packet size is constrained by the MTU, which is given as a signed short. */
        private const val MAX_PACKET_SIZE = Short.MAX_VALUE.toInt()

        /** Time to wait in between losing the connection and retrying. */
        private val MIN_RECONNECT_WAIT_MS = TimeUnit.SECONDS.toMillis(2)

        /** Time between keepalives if there is no traffic at the moment. */
        private val KEEPALIVE_INTERVAL_MS = TimeUnit.SECONDS.toMillis(15)

        /** Time to wait without receiving any response before assuming the server is gone. */
        private val RECEIVE_TIMEOUT_MS = TimeUnit.SECONDS.toMillis(20)

        /** Time between polling the VPN interface for new traffic, since it's non-blocking. */
        private val IDLE_INTERVAL_MS = TimeUnit.MILLISECONDS.toMillis(100)
        private val MIN_IDLE_INTERVAL_MS = TimeUnit.MILLISECONDS.toMillis(16)
        private val MAX_IDLE_INTERVAL_MS = TimeUnit.MILLISECONDS.toMillis(256)
    }

    private object Server {
        /**
         * Number of periods of length {@IDLE_INTERVAL_MS} to wait before declaring the
         * authenticateAndGetConfig a complete and abject failure.
         */
        private const val MAX_HANDSHAKE_ATTEMPTS = 50

        private fun parseParameters(parameters: String): ServerConfig? {
            var mtu = 0
            val addresses = mutableListOf<AddressWithPrefixLength>()
            val routes = mutableListOf<AddressWithPrefixLength>()
            val dnsServers = mutableListOf<String>()
            val searchDomains = mutableListOf<String>()
            for (parameter in parameters.split(" ")) {
                val fields = parameter.split(",")
                try {
                    when (fields[0][0]) {
                        'm' -> mtu = java.lang.Short.parseShort(fields[1]).toInt()
                        'a' -> addresses.add(AddressWithPrefixLength(fields[1], Integer.parseInt(fields[2])))
                        'r' -> routes.add(AddressWithPrefixLength(fields[1], Integer.parseInt(fields[2])))
                        'd' -> dnsServers.add(fields[1])
                        's' -> searchDomains.add(fields[1])
                    }
                } catch (e: NumberFormatException) {
                    return null
                }
            }
            return ServerConfig(mtu, addresses, routes, dnsServers, searchDomains)
        }

        fun authenticateAndGetConfig(channel: ByteChannel, secret: ByteArray): ServerConfig? {
            // To build a secured tunnel, we should perform mutual authentication
            // and exchange session keys for encryption. To keep things simple in
            // this demo, we just send the shared secret in plaintext and wait
            // for the server to send the parameters.

            // Allocate the buffer for handshaking. We have a hardcoded maximum
            // authenticateAndGetConfig size of 1024 bytes, which should be enough for demo
            // purposes.
            val packet = ByteBuffer.allocate(1024)

            // Control messages always start with zero.
            packet.put(0.toByte()).put(secret).flip()

            // Send the secret several times in case of packet loss.
            for (i in 0..2) {
                packet.position(0)
                channel.write(packet)
            }
            packet.clear()

            // Wait for the parameters within a limited time.
            for (i in 0 until MAX_HANDSHAKE_ATTEMPTS) {
                Thread.sleep(IDLE_INTERVAL_MS)

                // Normally we should not receive random packets. Check that the first
                // byte is 0 as expected.
                val length = channel.read(packet)
                if (length > 0 && packet.get(0).toInt() == 0) {
                    return String(packet.array(), 1, length - 1, US_ASCII)
                        .trim { it <= ' ' }
                        .let { parseParameters(it) }
                }
            }
            return null
        }
    }

    interface ConnectionEvents {
        fun onConnection(tunnel: ParcelFileDescriptor) = Unit
        fun onNoConnection() = Unit
        fun onStop() = Unit
    }

    private val tag = AppVpnConnection::class.java.simpleName
    private var executor: ExecutorService? = null

    @Volatile
    private var isRunning = false

    @Volatile
    private var connectedToVpn = false

    @Volatile
    private var reconnectWaitMs = MIN_RECONNECT_WAIT_MS

    val connectivityStats = ConnectivityStats()

    fun run() = synchronized(this) {
        if (!isRunning) {
            Log.i(tag, "run() isRunning: false")
            synchronized(service) { events.onNoConnection() }
            executor = executor ?: Executors.newFixedThreadPool(2)
            executor?.execute(runnable)
        } else {
            Log.i(tag, "run() isRunning: true")
            reconnectWaitMs = MIN_RECONNECT_WAIT_MS
        }
    }

    private fun disconnect() {
        if (connectedToVpn) {
            Log.i(tag, "disconnect() connectedToVpn: true")
            connectedToVpn = false
            synchronized(service) { events.onNoConnection() }
        }
    }

    fun stop() {
        connectedToVpn = false
        isRunning = false
    }

    fun shutDown() {
        executor?.shutdownNow()
    }

    private val runnable = Runnable {
        Log.i(tag, "Starting")
        isRunning = true

        try {
            var i = 0
            while (isRunning && i < 16) {
                val connected = startConnection(service, appConfig)

                // Reset the counter if we were connected.
                if (connected) {
                    i = 0
                    reconnectWaitMs = MIN_RECONNECT_WAIT_MS
                } else {
                    reconnectWaitMs += MIN_RECONNECT_WAIT_MS
                }

                // Sleep for a while before reconnecting
                Thread.sleep(reconnectWaitMs)
            }
        } catch (e: IOException) {
            Log.e(tag, "Connection failed, exiting", e)
        } catch (e: InterruptedException) {
            Log.e(tag, "Connection interrupted, exiting", e)
        } finally {
            Log.i(tag, "Giving/finishing up")
            stop()
            synchronized(service) { events.onStop() }
        }
    }

    private fun getChannel(service: VpnService, server: InetSocketAddress, isUdp: Boolean = true): ByteChannel? = try {
        if (isUdp) {
            // Create a DatagramChannel as the VPN tunnel using UDP
            DatagramChannel.open().apply {
                if (service.protect(socket())) {
                    connect(server)
                    configureBlocking(false)
                } else {
                    return null
                }
            }
        } else {
            // Create a SocketChannel as the VPN tunnel using TCP
            SocketChannel.open().apply {
                if (service.protect(socket())) {
                    connect(server)
                    configureBlocking(false)
                } else {
                    return null
                }
            }
        }
    } catch (e: IOException) {
        Log.e(tag, "Cannot create/configure channel", e)
        null
    }

    private fun startConnection(service: VpnService, app: AppVpnConfig): Boolean {
        val server = InetSocketAddress(app.serverAddress, app.serverPort)
        try {
            getChannel(service, server)?.use { tunnel ->
                Server.authenticateAndGetConfig(tunnel, app.serverSecret)
                    ?.let { VpnConfig(it, app) }
                    ?.let { service.getInterface(it) }
                    ?.use { netInterface ->
                        Log.i(tag, "Connected")
                        connectedToVpn = true
                        synchronized(service) { events.onConnection(netInterface) }
                        executor?.execute {
                            try {
                                writeToTunnel(netInterface, tunnel)
                            } catch (e: InterruptedException) {
                                Log.e(tag, "Writing thread interrupted, exiting", e)
                            }
                        }
                        readFromTunnel(netInterface, tunnel)
                        return true
                    }
            }
        } catch (e: SocketException) {
            Log.e(tag, "Cannot use socket", e)
        }
        return false
    }

    private fun readWriteTunnel(netInterface: ParcelFileDescriptor, tunnel: ByteChannel) {
        // Packets to be sent are queued in this input stream.
        val input = FileInputStream(netInterface.fileDescriptor)

        // Packets received need to be written to this output stream.
        val output = FileOutputStream(netInterface.fileDescriptor)

        // Allocate the buffer for a single packet.
        val packet = ByteBuffer.allocate(MAX_PACKET_SIZE)

        // Timeouts:
        //   - when data has not been sent in a while, send empty keepalive messages.
        //   - when data has not been received in a while, assume the connection is broken.
        var lastSendTime = System.currentTimeMillis()
        var lastReceiveTime = System.currentTimeMillis()

        // We keep forwarding packets till something goes wrong.
        while (isRunning) {
            // Assume that we did not make any progress in this iteration.
            var idle = true

            // Read the outgoing packet from the input stream.
            var length = try {
                input.read(packet.array())
            } catch (e: IOException) {
                break
            }
            if (length > 0) {
                // Write the outgoing packet to the tunnel.
                packet.limit(length)
                tunnel.write(packet)
                connectivityStats.totalRx += length
                packet.clear()

                // There might be more outgoing packets.
                idle = false
                lastReceiveTime = System.currentTimeMillis()
            }

            // Read the incoming packet from the tunnel.
            length = try {
                tunnel.read(packet)
            } catch (e: IOException) {
                break
            }
            if (length > 0) {
                // Ignore control messages, which start with zero.
                if (packet.get(0).toInt() != 0) {
                    // Write the incoming packet to the output stream.
                    output.write(packet.array(), 0, length)
                    connectivityStats.totalTx += length
                }
                packet.clear()

                // There might be more incoming packets.
                idle = false
                lastSendTime = System.currentTimeMillis()
            }

            // If we are idle or waiting for the network, sleep for a
            // fraction of time to avoid busy looping.
            if (idle) {
                Thread.sleep(IDLE_INTERVAL_MS)
                val timeNow = System.currentTimeMillis()

                if (lastSendTime + KEEPALIVE_INTERVAL_MS <= timeNow) {
                    // We are receiving for a long time but not sending.
                    // Send empty control messages.
                    packet.put(0.toByte()).limit(1)
                    for (i in 0..2) {
                        packet.position(0)
                        tunnel.write(packet)
                    }
                    packet.clear()
                    lastSendTime = timeNow
                } else if (lastReceiveTime + RECEIVE_TIMEOUT_MS <= timeNow) {
                    // We are sending for a long time but not receiving.
                    // throw IllegalStateException("Timed out")
                    Log.e(tag, "Receive time out")
                }
            }
        }
    }

    private fun readFromTunnel(netInterface: ParcelFileDescriptor, tunnel: ByteChannel) {
        var idleSleepTime = MIN_IDLE_INTERVAL_MS

        // Packets received need to be written to this output stream.
        val output = FileOutputStream(netInterface.fileDescriptor)

        // Allocate the buffer for a single packet.
        val packet = ByteBuffer.allocate(MAX_PACKET_SIZE)

        // We keep forwarding packets till something goes wrong.
        while (connectedToVpn) {
            // Read the incoming packet from the tunnel.
            val length = try {
                tunnel.read(packet)
            } catch (e: IOException) {
                disconnect()
                break
            }
            if (length > 0) {
                // Ignore control messages, which start with zero.
                if (packet.get(0).toInt() != 0) {
                    // Write the incoming packet to the output stream.
                    try {
                        output.write(packet.array(), 0, length)
                        connectivityStats.totalTx += length
                    } catch (e: IOException) {
                        disconnect()
                        break
                    }
                }
                packet.clear()
                idleSleepTime = MIN_IDLE_INTERVAL_MS
            } else {
                // If we are idle or waiting for the network, sleep for a
                // fraction of time to avoid busy looping.
                idleSleepTime = if (idleSleepTime < MAX_IDLE_INTERVAL_MS) (idleSleepTime * 2) else idleSleepTime
                Thread.sleep(idleSleepTime)
            }
        }
    }

    private fun writeToTunnel(netInterface: ParcelFileDescriptor, tunnel: ByteChannel) {
        var idleSleepTime = MIN_IDLE_INTERVAL_MS

        // Packets to be sent are queued in this input stream.
        val input = FileInputStream(netInterface.fileDescriptor)

        // Allocate the buffer for a single packet.
        val packet = ByteBuffer.allocate(MAX_PACKET_SIZE)

        // We keep forwarding packets till something goes wrong.
        while (connectedToVpn) {
            // Read the outgoing packet from the input stream.
            val length = try {
                input.read(packet.array())
            } catch (e: IOException) {
                disconnect()
                break
            }
            if (length > 0) {
                // Write the outgoing packet to the tunnel.
                packet.limit(length)
                try {
                    tunnel.write(packet)
                    connectivityStats.totalRx += length
                } catch (e: IOException) {
                    disconnect()
                    break
                }
                packet.clear()
                idleSleepTime = MIN_IDLE_INTERVAL_MS
            } else {
                // If we are idle or waiting for the network, sleep for a
                // fraction of time to avoid busy looping.
                idleSleepTime = if (idleSleepTime < MAX_IDLE_INTERVAL_MS) (idleSleepTime * 2) else idleSleepTime
                Thread.sleep(idleSleepTime)
            }
        }
    }
}
