package com.softograph.vpnapp.service

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.VpnService
import android.os.*
import androidx.core.app.NotificationCompat
import android.util.Log
import android.widget.Toast
import com.softograph.vpnapp.R
import com.softograph.vpnapp.data.SharedPrefManager.getAppConfig
import com.softograph.vpnapp.data.VpnConfig
import com.softograph.vpnapp.helper.BroadCastHelper
import com.softograph.vpnapp.helper.BroadCastHelper.broadcastVpnConnected
import com.softograph.vpnapp.helper.BroadCastHelper.broadcastVpnConnecting
import com.softograph.vpnapp.helper.BroadCastHelper.broadcastVpnDisconnected
import com.softograph.vpnapp.helper.BroadCastHelper.broadcastVpnStats
import com.softograph.vpnapp.helper.ConnectionStatus
import com.softograph.vpnapp.helper.NetworkChangeReceiver
import com.softograph.vpnapp.view.HomeActivity

class AppVpnService : VpnService() {
    companion object {
        private val TAG = AppVpnService::class.java.simpleName
        const val ACTION_DISCONNECT = "com.softograph.vpnapp.STOP"
        const val STATS_INTERVAL_S = 5
        const val STATS_INTERVAL_MS = STATS_INTERVAL_S * 1000L
        const val CONNECTION_TIMEOUT_MS = 40 * 1000L

        fun getPendingIntent(context: Context): PendingIntent {
            val intent = Intent(context, HomeActivity::class.java)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }
    }

    private var isConnected = ConnectionStatus.DISCONNECTED
    private var baseTime = 0L
    private fun resetBaseTime() {
        baseTime = SystemClock.elapsedRealtime()
    }

    private val uiActivatedReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            Log.i("Broadcast", "uiActivatedReceiver onReceive()")
            when (isConnected) {
                ConnectionStatus.CONNECTED -> broadcastVpnConnected(baseTime)
                ConnectionStatus.CONNECTING -> broadcastVpnConnecting()
            }
        }
    }

    private var vpnConnectedOnce = false
    private val onlineStatusReceiver = NetworkChangeReceiver()

    private var vpnConnection: AppVpnConnection? = null

    private val messageHandler = Handler { message ->
        Toast.makeText(this, message.what, Toast.LENGTH_SHORT).show()
        updateForegroundNotification(message.what)
        true
    }

    private var statsSendingStarted = false

    inner class ConnectionListener : AppVpnConnection.ConnectionEvents {
        override fun onConnection(tunnel: ParcelFileDescriptor) {
            isConnected = ConnectionStatus.CONNECTED
            resetBaseTime()
            broadcastVpnConnected(baseTime)
            messageHandler.sendEmptyMessage(R.string.connected_notification)
            startSendingStats()
            vpnConnectedOnce = true
        }

        override fun onNoConnection() {
            isConnected = ConnectionStatus.CONNECTING
            broadcastVpnConnecting()
            messageHandler.sendEmptyMessage(R.string.connecting_notification)
        }

        override fun onStop() {
            isConnected = ConnectionStatus.DISCONNECTED
            broadcastVpnDisconnected()
            messageHandler.sendEmptyMessage(R.string.disconnected_notification)
        }
    }

    override fun onCreate() {
        Log.i(TAG, "onCreate()")
        updateForegroundNotification(R.string.connecting_notification)
        registerReceiver(onlineStatusReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        registerReceiver(uiActivatedReceiver, IntentFilter(BroadCastHelper.UI_ACTIVATED))
        onlineStatusReceiver.onAvailable = {
            Log.i(TAG, "onlineStatusReceiver.onAvailable")
            if (vpnConnectedOnce) {
                vpnConnection?.run()
            }
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i(TAG, "onStartCommand()")
        if (intent?.action == ACTION_DISCONNECT) {
            Log.i(TAG, "onStartCommand() ACTION_DISCONNECT")
            vpnConnection?.stop()
            stopSelf(startId)
            Service.START_NOT_STICKY
        } else {
            val appConfig = getAppConfig()
            if (appConfig != null) {
                vpnConnection = vpnConnection ?: AppVpnConnection(this, appConfig, ConnectionListener())
                vpnConnection?.run()
                messageHandler.postDelayed({
                    if (isConnected == ConnectionStatus.CONNECTING) {
                        Log.i(TAG, "Stop trying to connect after timeout")
                        vpnConnection?.stop()
                    }
                }, CONNECTION_TIMEOUT_MS)
            } else {
                Log.e(TAG, "appConfig is NULL. Cannot start!")
            }
        }
        return Service.START_STICKY
    }

    override fun onDestroy() {
        Log.i(TAG, "onDestroy()")
        vpnConnection?.stop()
        vpnConnection?.shutDown()
        vpnConnection = null
        onlineStatusReceiver.clearEvents()
        unregisterReceiver(onlineStatusReceiver)
        unregisterReceiver(uiActivatedReceiver)
    }

    private fun updateForegroundNotification(messageResId: Int) {
        val notificationId = 789
        val pendingIntent = getPendingIntent(this)

        val channelID = "AvaVpn"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(channelID, channelID, NotificationManager.IMPORTANCE_DEFAULT)
            val manager = (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
            manager.createNotificationChannel(channel)
        }
        val notification = NotificationCompat.Builder(this, channelID)
            .setSmallIcon(R.drawable.ic_vpn_lock)
            .setContentText(getString(messageResId))
            .setContentIntent(pendingIntent)
            .setOngoing(true)
            .build()
        notification.flags = notification.flags or Notification.FLAG_NO_CLEAR
        startForeground(notificationId, notification)
    }

    private fun startSendingStats() {
        if (statsSendingStarted) return

        val runnable = object : Runnable {
            override fun run() {
                if (isConnected == ConnectionStatus.CONNECTED) {
                    vpnConnection?.connectivityStats?.also {
                        val rxSpeed = (it.totalRx - it.lastRx) / STATS_INTERVAL_S
                        val txSpeed = (it.totalTx - it.lastTx) / STATS_INTERVAL_S
                        it.lastRx = it.totalRx
                        it.lastTx = it.totalTx
                        broadcastVpnStats(it.totalRx, it.totalTx, rxSpeed.toInt(), txSpeed.toInt())
                    }
                }
                messageHandler.postDelayed(this, STATS_INTERVAL_MS)
            }
        }
        messageHandler.post(runnable)
        statsSendingStarted = true
    }
}

fun VpnService.getInterface(vpnConfig: VpnConfig): ParcelFileDescriptor? {
    val builder = Builder().setMtu(vpnConfig.server.mtu)
    vpnConfig.server.addresses.forEach {
        builder.addAddress(it.address, it.prefixLength)
    }
    vpnConfig.server.routes.forEach { route ->
        builder.addRoute(route.address, route.prefixLength)
    }
    vpnConfig.server.dnsServers.forEach { builder.addDnsServer(it) }
    vpnConfig.server.searchDomains.forEach { builder.addSearchDomain(it) }

    vpnConfig.app.disallowedPackages.forEach { packageName ->
        try {
            builder.addDisallowedApplication(packageName)
        } catch (e: PackageManager.NameNotFoundException) {
            Log.w("VpnService.getInterface", "Package not available: $packageName", e)
        }
    }

    builder
        .setSession(vpnConfig.app.serverAddress)
        .setConfigureIntent(AppVpnService.getPendingIntent(this))

    return synchronized(this) { builder.establish() }
}
