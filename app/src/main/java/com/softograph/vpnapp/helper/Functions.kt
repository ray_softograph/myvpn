package com.softograph.vpnapp.helper

import android.app.Activity
import android.app.AlarmManager
import android.app.PendingIntent
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.SystemClock
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import com.softograph.vpnapp.R
import com.softograph.vpnapp.service.AppVpnService


object ConnectionStatus {
    const val CONNECTED = 1
    const val DISCONNECTED = -1
    const val CONNECTING = 0
}

fun Activity.showProgressDialog(s: String): ProgressDialog {
    val dialog = ProgressDialog(this)
    dialog.setMessage(s)
    dialog.setCancelable(false)
    dialog.show()
    return dialog
}

fun ProgressDialog.hideProgressDialog() {
    if (this.isShowing) {
        this.dismiss()
    }
}

fun Context.startVpnService() {
    val serviceIntent = Intent(applicationContext, AppVpnService::class.java)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        startForegroundService(serviceIntent)
    } else {
        startService(serviceIntent)
    }
}

fun Context.startServiceFromAlarmManager() {
    val intent = Intent(applicationContext, AppVpnService::class.java)
    val pendingIntent = PendingIntent.getService(this, 1, intent, PendingIntent.FLAG_ONE_SHOT)
    val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
    alarmManager.set(AlarmManager.RTC_WAKEUP, SystemClock.elapsedRealtime() + 5000, pendingIntent)
}

fun Context.showYesNoPrompt(@StringRes textId: Int, positiveAction: () -> Unit) {
    AlertDialog.Builder(this)
        .setMessage(textId)
        .setPositiveButton(R.string.prompt_yes) { _, _ -> positiveAction.invoke() }
        .setNegativeButton(R.string.prompt_no, null)
        .show()
}
