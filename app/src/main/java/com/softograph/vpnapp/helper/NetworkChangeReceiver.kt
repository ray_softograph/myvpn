package com.softograph.vpnapp.helper

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager

class NetworkChangeReceiver : BroadcastReceiver() {
    var onAvailable: (() -> Unit)? = null
    var onLost: (() -> Unit)? = null

    override fun onReceive(context: Context, intent: Intent?) {
        val connMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val isOnline = (connMgr.activeNetworkInfo != null)
        if (isOnline) {
            onAvailable?.invoke()
        } else {
            onLost?.invoke()
        }
    }

    fun clearEvents() {
        onAvailable = null
        onLost = null
    }
}