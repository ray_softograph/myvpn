package com.softograph.vpnapp.helper

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

/**
 * This static class provides executor service for background tasks
 */
object BackgroundService {
    private const val N_THREADS = 4

    private val executorService: ExecutorService by lazy {
        Executors.newFixedThreadPool(N_THREADS)
    }

    fun <T> submit(task: (() -> T)): Future<T> {
        return executorService.submit(task)
    }
}
