package com.softograph.vpnapp.helper

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log


class AppVpnServiceStarter : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        Log.i("AppVpnServiceStarter", "Staring the service..")
        context.startVpnService()
    }
}
