package com.softograph.vpnapp.helper

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.util.AttributeSet

class ExpandedRecycleView(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0)
    : RecyclerView(context, attrs, defStyle) {

    private var expanded = false

    fun setExpanded(value: Boolean) {
        expanded = value
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (expanded) {
            val expandSpec = MeasureSpec.makeMeasureSpec(Int.MAX_VALUE shr 2, MeasureSpec.AT_MOST)
            super.onMeasure(widthMeasureSpec, expandSpec)

            layoutParams.height = measuredHeight
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        }
    }
}