package com.softograph.vpnapp.helper

import android.content.Context
import android.content.Intent
import android.util.Log

object BroadCastHelper {
    private const val PREFIX = "COM.SOFTOGRAPH.AVA."

    const val UI_ACTIVATED = PREFIX + "UI_ACTIVATED"
    const val VPN_CONNECTION = PREFIX + "VPN_CONNECTION"
    const val VPN_CONNECTION_STATS = PREFIX + "VPN_CONNECTION_STATS"

    private const val VPN_CONNECTED = PREFIX + "VPN_CONNECTED"
    private const val VPN_RX = PREFIX + "VPN_RX"
    private const val VPN_TX = PREFIX + "VPN_TX"
    private const val VPN_SPEED_RX = PREFIX + "VPN_SPEED_RX"
    private const val VPN_SPEED_TX = PREFIX + "VPN_SPEED_TX"
    private const val BASE_TIME = PREFIX + "BASE_TIME"


    fun Context.broadcastUiActivated() {
        Intent(UI_ACTIVATED).also {
            sendBroadcast(it)
        }
        Log.i("Broadcast", "broadcastUiActivated()")
    }

    fun Context.broadcastVpnConnected(baseTime: Long) {
        Intent(VPN_CONNECTION).also {
            it.putExtra(VPN_CONNECTED, ConnectionStatus.CONNECTED)
            it.putExtra(BASE_TIME, baseTime)
            sendBroadcast(it)
        }
        Log.i("Broadcast", "broadcastVpnConnected()")
    }

    fun Context.broadcastVpnConnecting() {
        Intent(VPN_CONNECTION).also {
            it.putExtra(VPN_CONNECTED, ConnectionStatus.CONNECTING)
            sendBroadcast(it)
        }
        Log.i("Broadcast", "broadcastVpnDisconnected()")
    }

    fun Context.broadcastVpnDisconnected() {
        Intent(VPN_CONNECTION).also {
            it.putExtra(VPN_CONNECTED, ConnectionStatus.DISCONNECTED)
            sendBroadcast(it)
        }
        Log.i("Broadcast", "broadcastVpnDisconnected()")
    }

    fun Context.broadcastVpnStats(rx: Long, tx: Long, rxSpeed: Int, txSpeed: Int) {
        Intent(VPN_CONNECTION_STATS).also {
            it.putExtra(VPN_RX, rx)
            it.putExtra(VPN_TX, tx)
            it.putExtra(VPN_SPEED_RX, rxSpeed)
            it.putExtra(VPN_SPEED_TX, txSpeed)
            sendBroadcast(it)
        }
        Log.i("Broadcast", "broadcastVpnStats() rx:$rx, tx:$tx, rxSpeed:$rxSpeed, txSpeed:$txSpeed")
    }

    fun Intent.isVpnConnected() = getIntExtra(VPN_CONNECTED, ConnectionStatus.DISCONNECTED)
    fun Intent.getBaseTime() = getLongExtra(BASE_TIME, -1L)
    fun Intent.getVpnRx() = getLongExtra(VPN_RX, 0L)
    fun Intent.getVpnTx() = getLongExtra(VPN_TX, 0L)
    fun Intent.getVpnRxSpeed() = getIntExtra(VPN_SPEED_RX, 0)
    fun Intent.getVpnTxSpeed() = getIntExtra(VPN_SPEED_TX, 0)
}