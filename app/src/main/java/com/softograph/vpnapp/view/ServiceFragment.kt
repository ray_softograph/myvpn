package com.softograph.vpnapp.view


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.text.format.Formatter
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.softograph.vpnapp.R
import com.softograph.vpnapp.data.SharedPrefManager.getAppConfig
import com.softograph.vpnapp.data.SharedPrefManager.getUser
import kotlinx.android.synthetic.main.fragment_service.*
import java.net.NetworkInterface
import java.net.SocketException


class ServiceFragment : Fragment() {

    private val mTag = "ServiceFragment"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_service, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getLocalIpAddress()?.let { tvIP.text = it }
        view.context.getUser()?.let { tvUserName.text = it }
        view.context.getAppConfig()?.let {
            tvAddress.text = it.serverAddress
            tvRoute.text = it.serverPort.toString()
        }
    }

    private fun getLocalIpAddress(): String? {
        try {
            NetworkInterface.getNetworkInterfaces().asSequence().forEach { networkInterface ->
                networkInterface.inetAddresses.asSequence().forEach { inetAddress ->
                    if (!inetAddress.isLoopbackAddress) {
                        val ip = Formatter.formatIpAddress(inetAddress.hashCode())
                        Log.i(mTag, "Device IP=$ip")
                        return ip
                    }
                }
            }
        } catch (ex: SocketException) {
            Log.e(mTag, "Cannot obtain IP address", ex)
        }
        return null
    }
}
