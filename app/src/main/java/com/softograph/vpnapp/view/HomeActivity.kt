package com.softograph.vpnapp.view

import android.app.Activity
import android.app.AlertDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.VpnService
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import com.softograph.vpnapp.R
import com.softograph.vpnapp.data.SharedPrefManager.clearAppConfig
import com.softograph.vpnapp.helper.*
import com.softograph.vpnapp.helper.BroadCastHelper.broadcastUiActivated
import com.softograph.vpnapp.helper.BroadCastHelper.getBaseTime
import com.softograph.vpnapp.helper.BroadCastHelper.getVpnRx
import com.softograph.vpnapp.helper.BroadCastHelper.getVpnRxSpeed
import com.softograph.vpnapp.helper.BroadCastHelper.getVpnTx
import com.softograph.vpnapp.helper.BroadCastHelper.getVpnTxSpeed
import com.softograph.vpnapp.helper.BroadCastHelper.isVpnConnected
import com.softograph.vpnapp.service.AppVpnService
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), StatusFragment.Listener, VpnStartController {
    private val tag = "HomeActivity"
    private var statusFragment: StatusFragment? = null

    private var isOnline = false
    private var isConnected = ConnectionStatus.DISCONNECTED
    private var baseTime = 0L

    private val onlineStatusReceiver = NetworkChangeReceiver()

    override fun isOnline(): Boolean = isOnline
    override fun isConnected(): Int = isConnected
    override fun getBaseTime(): Long = baseTime

    private val vpnConnectionReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when (intent.action) {
                BroadCastHelper.VPN_CONNECTION -> {
                    isConnected = intent.isVpnConnected()
                    Log.i("Broadcast", "vpnConnectionReceiver onReceive() isConnected: $isConnected")
                    intent.getBaseTime().also {
                        if (it != -1L) {
                            baseTime = it
                        }
                    }
                    when (isConnected) {
                        ConnectionStatus.CONNECTED -> statusFragment?.onVpnConnected(baseTime)
                        ConnectionStatus.CONNECTING -> statusFragment?.onVpnConnecting()
                        ConnectionStatus.DISCONNECTED -> statusFragment?.onVpnDisconnected()
                    }
                }
                BroadCastHelper.VPN_CONNECTION_STATS -> {
                    val rx = intent.getVpnRx()
                    val tx = intent.getVpnTx()
                    val rxSpeed = intent.getVpnRxSpeed()
                    val txSpeed = intent.getVpnTxSpeed()
                    statusFragment?.setStats(rx, tx, rxSpeed, txSpeed)
                }
            }
        }
    }

    override fun setStatusFragment(fragment: StatusFragment) {
        statusFragment = fragment
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(home_toolbar as Toolbar)

        mainViewPager.adapter = HomePagerAdapter(supportFragmentManager, this)
        tabLayout.setupWithViewPager(mainViewPager)

        registerReceiver(onlineStatusReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
        onlineStatusReceiver.onAvailable = { isOnline = true }
        onlineStatusReceiver.onLost = { isOnline = false }

        registerReceiver(
            vpnConnectionReceiver,
            IntentFilter(BroadCastHelper.VPN_CONNECTION).apply { addAction(BroadCastHelper.VPN_CONNECTION_STATS) }
        )
        broadcastUiActivated()
    }

    override fun onDestroy() {
        Log.i(tag, "onDestroy()")
        onlineStatusReceiver.clearEvents()
        unregisterReceiver(onlineStatusReceiver)
        unregisterReceiver(vpnConnectionReceiver)
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            menu?.findItem(R.id.m_always_on)?.isVisible = false
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.m_logout -> {
                AlertDialog.Builder(this)
                    .setTitle("Logout")
                    .setMessage("Are sure you want to logout?")
                    .setPositiveButton("Logout") { _, _ -> logout() }
                    .setNegativeButton("Cancel", null)
                    .show()
            }
            R.id.m_always_on -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    showYesNoPrompt(R.string.always_on_prompt) {
                        startActivityForResult(Intent(android.provider.Settings.ACTION_VPN_SETTINGS), 0)
                    }
                }
            }
        }
        return true
    }

    private fun logout() {
        clearAppConfig()
        disconnectVpnService()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    override fun checkAndStartVpnService() {
        val intent = VpnService.prepare(this)
        if (intent != null) {
            startActivityForResult(intent, 0)
        } else {
            onActivityResult(0, Activity.RESULT_OK, null)
        }
    }

    override fun disconnectVpnService() {
        Intent(this, AppVpnService::class.java)
            .setAction(AppVpnService.ACTION_DISCONNECT)
            .let { startService(it) }
    }

    override fun restartVpnService() {
        if (isConnected != ConnectionStatus.CONNECTED) return

        disconnectVpnService()
        Handler().postDelayed({
            checkAndStartVpnService()
        }, 2048)
    }

    override fun onActivityResult(request: Int, result: Int, data: Intent?) {
        if (result == AppCompatActivity.RESULT_OK) {
            startVpnService()
            isConnected = ConnectionStatus.CONNECTING
        }
    }

    inner class HomePagerAdapter(fm: FragmentManager, private val context: Context) : FragmentPagerAdapter(fm) {

        override fun getCount(): Int = 3

        override fun getItem(position: Int): Fragment = when (position) {
            0 -> StatusFragment()
            1 -> ServiceFragment()
            else -> AppsFragment()
        }

        override fun getPageTitle(position: Int): CharSequence? = when (position) {
            0 -> context.getString(R.string.status)
            1 -> context.getString(R.string.service)
            else -> context.getString(R.string.apps)
        }
    }
}

interface VpnStartController {
    fun checkAndStartVpnService()
    fun disconnectVpnService()
    fun restartVpnService()
}
