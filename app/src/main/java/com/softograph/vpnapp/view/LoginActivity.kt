package com.softograph.vpnapp.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.widget.ArrayAdapter
import android.widget.Toast
import com.softograph.vpnapp.R
import com.softograph.vpnapp.data.ApiFactory
import com.softograph.vpnapp.data.ConfigFromServer
import com.softograph.vpnapp.data.SharedPrefManager.getAppConfig
import com.softograph.vpnapp.data.SharedPrefManager.saveAppConfig
import com.softograph.vpnapp.data.SharedPrefManager.saveUser
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception


class LoginActivity : AppCompatActivity() {

    private val ids by lazy {
        listOf(
            R.string.select_acc,
            R.string.user_1,
            R.string.user_2,
            R.string.user_3
        ).map { getString(it) }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val appConfig = getAppConfig()
        if (appConfig != null) {
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }

        spinner.adapter = ArrayAdapter(this, R.layout.spinner_text_view, ids)
        btnLogIn.setOnClickListener {
            progressbar.visibility = View.VISIBLE

            window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            )

            if (spinner.selectedItemPosition < 1 || etPinPin.text?.isEmpty() == true) {
                progressbar.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                AlertDialog.Builder(this)
                    .setMessage("Please select user and enter password properly and try again")
                    .setPositiveButton("Retry", null)
                    .show()
            } else {
                login(ids[spinner.selectedItemPosition], etPinPin.text.toString())
            }
        }
    }

    private fun login(userId: String, password: String) {
        val map = mapOf("username" to userId, "password" to password)

        ApiFactory.Factory.getService().login(map).enqueue(object : Callback<ConfigFromServer> {
            override fun onFailure(call: Call<ConfigFromServer>, t: Throwable) {
                progressbar.visibility = View.GONE
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                Toast.makeText(this@LoginActivity, t.toString(), Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ConfigFromServer>, response: Response<ConfigFromServer>) {
                val context = this@LoginActivity
                val responseBody = response.body()
                if (response.code() == 200 && responseBody != null) {
                    progressbar.visibility = View.GONE
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                    context.saveAppConfig(responseBody)
                    context.saveUser(userId)
                    startActivity(Intent(context, HomeActivity::class.java))
                    finish()
                } else {
                    val localErrorMessage = "Cannot Sign in"
                    val errorMessage = try {
                        response.errorBody()?.string()
                            ?.let { JSONObject(it).getString("message") }
                            ?: localErrorMessage
                    } catch (e: Exception) {
                        localErrorMessage
                    }

                    Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show()
                    progressbar.visibility = View.GONE
                    window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
                }
            }
        })
    }
}
