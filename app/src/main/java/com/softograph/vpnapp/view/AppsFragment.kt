package com.softograph.vpnapp.view

import android.content.Context
import android.content.pm.PackageInfo
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Switch
import android.widget.TextView
import com.softograph.vpnapp.R
import com.softograph.vpnapp.data.SharedPrefManager.getDisallowedPackages
import com.softograph.vpnapp.data.SharedPrefManager.removeFromDisallowedPackages
import com.softograph.vpnapp.data.SharedPrefManager.saveToDisallowedPackages
import com.softograph.vpnapp.helper.BackgroundService


class AppsFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private val vpnController by lazy { activity as VpnStartController }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_apps, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view as RecyclerView
        loadData(view.context)
    }

    private fun loadData(context: Context) {
        val disallowedPackages = context.getDisallowedPackages()
        Log.i("LoadData", "getting allApps")
        val allApps = context.packageManager.getInstalledPackages(0).filter { it.versionName != null }

        val allowedApps = mutableListOf<PackageInfo>()
        val disallowedApps = mutableListOf<PackageInfo>()

        allApps.forEach {
            if (disallowedPackages.contains(it.packageName)) {
                disallowedApps.add(it)
            } else {
                allowedApps.add(it)
            }
        }
        allowedApps.sortBy { it.packageName }
        disallowedApps.sortBy { it.packageName }

        recyclerView.adapter = AppsAdapter(disallowedApps, allowedApps, getListener())
        Log.i("LoadData", "done")
    }

    private fun getListener() = object : AppsAdapter.Listener {
        override fun reloadAndRestartService(context: Context) {
            val pageView = view ?: return
            val snackBar = Snackbar
                .make(pageView, R.string.restart_vpn_prompt, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.restart) {
                    vpnController.restartVpnService()
                    recyclerView.adapter?.notifyDataSetChanged()
                }
            snackBar.show()
        }
    }
}

class AppsAdapter(
    private val disallowedApps: MutableList<PackageInfo>,
    private val allowedApps: MutableList<PackageInfo>,
    private val listener: Listener
) :
    RecyclerView.Adapter<AppViewHolder>() {

    companion object {
        private const val ITEM_TYPE_HEADER = 2
        private const val ITEM_TYPE_APP = 1
    }

    override fun getItemCount(): Int {
        val disallowedSize = if (disallowedApps.isEmpty()) 0 else (disallowedApps.size + 1)
        return disallowedSize + allowedApps.size + 1
    }

    override fun getItemViewType(position: Int): Int {
        val allowedHeaderPosition = if (disallowedApps.isEmpty()) 0 else (disallowedApps.size + 1)
        return if (position == 0 || position == allowedHeaderPosition)
            ITEM_TYPE_HEADER
        else ITEM_TYPE_APP
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppViewHolder {
        val layout =
            if (viewType == ITEM_TYPE_HEADER) R.layout.header_item_layout
            else R.layout.app_item_layout
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return if (viewType == ITEM_TYPE_HEADER) AppViewHolder.Header(view)
        else AppViewHolder.Item(view)
    }

    override fun onBindViewHolder(holder: AppViewHolder, position: Int) {
        val context = holder.itemView.context
        if (disallowedApps.isEmpty()) {
            if (position == 0) {
                val headerText = context.getString(R.string.n_app_are_using_vpn, allowedApps.size)
                (holder as AppViewHolder.Header).textView.text = headerText
            } else {
                val item = allowedApps[position - 1]
                displayApp(holder as AppViewHolder.Item, item, true)
            }
        } else {
            when {
                position == 0 -> {
                    val headerText = context.getString(R.string.n_app_are_not_using_vpn, disallowedApps.size)
                    (holder as AppViewHolder.Header).textView.text = headerText
                }
                position <= disallowedApps.size -> {
                    val item = disallowedApps[position - 1]
                    displayApp(holder as AppViewHolder.Item, item, false)
                }
                position == disallowedApps.size + 1 -> {
                    val headerText = context.getString(R.string.n_app_are_using_vpn, allowedApps.size)
                    (holder as AppViewHolder.Header).textView.text = headerText
                }
                position <= disallowedApps.size + allowedApps.size + 1 -> {
                    val item = allowedApps[position - disallowedApps.size - 2]
                    displayApp(holder as AppViewHolder.Item, item, true)
                }
            }
        }
        Log.i("LoadData", "loading item for $position done")
    }

    private fun displayApp(holder: AppViewHolder.Item, item: PackageInfo, isAllowed: Boolean) {
        val context = holder.itemView.context
        val packageMan = context.packageManager

        holder.tvAppName.text = item.packageName
        BackgroundService.submit {
            item.applicationInfo.loadLabel(packageMan).also {
                holder.itemView.post { holder.tvAppName.text = it }
            }
            item.applicationInfo.loadIcon(packageMan).also {
                holder.itemView.post { holder.tvIcon.setImageDrawable(it) }
            }
        }
        holder.swApp.isChecked = isAllowed
        holder.swApp.setOnClickListener {
            val isChecked = (it as Switch).isChecked
            if (isChecked) {
                context.removeFromDisallowedPackages(item.packageName)
                disallowedApps.remove(item)
                allowedApps.add(item)
            } else {
                context.saveToDisallowedPackages(item.packageName)
                allowedApps.remove(item)
                disallowedApps.add(item)
            }
            listener.reloadAndRestartService(context)
        }
    }

    interface Listener {
        fun reloadAndRestartService(context: Context)
    }
}

sealed class AppViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    class Header(itemView: View) : AppViewHolder(itemView) {
        val textView = itemView as TextView
    }

    class Item(itemView: View) : AppViewHolder(itemView) {
        val tvAppName: TextView = itemView.findViewById(R.id.tvAppName)
        val tvIcon: ImageView = itemView.findViewById(R.id.iv_icon)
        val swApp: Switch = itemView.findViewById(R.id.sw_vpn)
    }
}
