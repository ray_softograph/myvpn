package com.softograph.vpnapp.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.softograph.vpnapp.R
import com.softograph.vpnapp.helper.ConnectionStatus
import com.softograph.vpnapp.helper.showYesNoPrompt
import kotlinx.android.synthetic.main.fragment_status.*

class StatusFragment : Fragment() {

    private val listener by lazy { activity as Listener }
    private val vpnController by lazy { activity as VpnStartController }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_status, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        listener.setStatusFragment(this)
        when (listener.isConnected()) {
            ConnectionStatus.CONNECTED -> {
                setViewConnected()
                chronometer.base = listener.getBaseTime()
                chronometer.start()
            }
            ConnectionStatus.CONNECTING -> setViewConnecting()
            ConnectionStatus.DISCONNECTED -> setViewDisconnected()
        }

        btnConnect.setOnClickListener {
            when (listener.isConnected()) {
                ConnectionStatus.DISCONNECTED -> {
                    if (listener.isOnline()) {
                        vpnController.checkAndStartVpnService()
                        setViewConnecting()
                    } else {
                        Toast.makeText(context, R.string.you_are_offline, Toast.LENGTH_SHORT).show()
                    }
                }
                ConnectionStatus.CONNECTING -> {
                    Toast.makeText(context, R.string.wait_while_connecting, Toast.LENGTH_SHORT).show()
                }
                ConnectionStatus.CONNECTED -> {
                    context?.showYesNoPrompt(R.string.disconnect_prompt) {
                        vpnController.disconnectVpnService()
                        setViewDisconnected()
                        chronometer.stop()
                    }
                }
            }
        }
    }

    private fun setViewConnected() {
        btnConnect.background = resources.getDrawable(R.drawable.vpn_switch_on, null)
        statLayout.visibility = View.VISIBLE
        tvStatusON.visibility = View.GONE
        tvStatusOff.visibility = View.VISIBLE
        chronometer.visibility = View.VISIBLE
        tvConnect.setText(R.string.connected)
    }

    private fun setViewConnecting() {
        btnConnect.background = resources.getDrawable(R.drawable.vpn_switch_off, null)
        statLayout.visibility = View.GONE
        tvStatusOff.visibility = View.GONE
        chronometer.visibility = View.GONE
        tvStatusON.setText(R.string.please_wait)
        tvStatusON.visibility = View.VISIBLE
        tvConnect.setText(R.string.connecting)
    }

    private fun setViewDisconnected() {
        btnConnect.background = resources.getDrawable(R.drawable.vpn_switch_off, null)
        statLayout.visibility = View.GONE
        tvStatusOff.visibility = View.GONE
        chronometer.visibility = View.GONE
        tvStatusON.setText(R.string.on)
        tvStatusON.visibility = View.VISIBLE
        tvConnect.setText(R.string.disconnected)
    }

    fun onVpnConnected(baseTime: Long) {
        view ?: return

        setViewConnected()
        chronometer.base = baseTime
        chronometer.start()
    }

    fun onVpnConnecting() {
        view ?: return

        setViewConnecting()
    }

    fun onVpnDisconnected() {
        view ?: return

        setViewDisconnected()
        chronometer.stop()
    }

    fun setStats(rx: Long, tx: Long, rxSpeed: Int, txSpeed: Int) {
        view ?: return

        txTextView.text = getString(R.string.n_KB, (tx / 1024))
        rxTextView.text = getString(R.string.n_KB, (rx / 1024))
        uplinkTextView.text = getString(R.string.n_kbps, (txSpeed.toDouble() / 1024))
        downlinkTextView.text = getString(R.string.n_kbps, (rxSpeed.toDouble() / 1024))
    }

    interface Listener {
        fun isOnline(): Boolean
        fun isConnected(): Int
        fun getBaseTime(): Long
        fun setStatusFragment(fragment: StatusFragment)
    }
}
