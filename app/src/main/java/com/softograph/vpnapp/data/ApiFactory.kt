package com.softograph.vpnapp.data

import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import java.util.concurrent.TimeUnit

interface ApiFactory {
    @FormUrlEncoded
    @POST("vpn_login/login.php")
    fun login(@FieldMap userPrams: Map<String, String>): Call<ConfigFromServer>


    object Factory {
        private const val BASE_URL = "https://www.softograph.com/"

        private var service: ApiFactory? = null

        fun getService(): ApiFactory {
            fun okHttpClient() = OkHttpClient().newBuilder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .build()

            return service ?: createService(okHttpClient())
        }

        private fun createService(okHttpClient: OkHttpClient): ApiFactory {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .baseUrl(BASE_URL).build()
            return retrofit.create(ApiFactory::class.java).also { service = it }
        }


    }
}