package com.softograph.vpnapp.data

import android.content.Context


object SharedPrefManager {
    private const val NAME = "connection"
    private const val SERVER_ADDRESS = "server.address"
    private const val SERVER_PORT = "server.port"
    private const val SHARED_SECRET = "shared.secret"
    private const val PACKAGES = "disallowed.packages"
    private const val USERID = "userid"

    private fun Context.preferences() = getSharedPreferences(NAME, Context.MODE_MULTI_PROCESS)

    private fun Context.getDisallowedPackagesSet(): Set<String> =
        preferences().getStringSet(PACKAGES, null) ?: setOf()

    fun Context.getDisallowedPackages(): List<String> =
        getDisallowedPackagesSet().toList()

    fun Context.saveToDisallowedPackages(packageName: String) {
        val newSet = getDisallowedPackagesSet() + packageName
        preferences().edit()
            .putStringSet(PACKAGES, newSet)
            .apply()
    }

    fun Context.removeFromDisallowedPackages(packageName: String) {
        val newSet = getDisallowedPackagesSet() - packageName
        preferences().edit()
            .putStringSet(PACKAGES, newSet)
            .apply()
    }

    fun Context.getAppConfig(): AppVpnConfig? {
        val server = preferences().getString(SERVER_ADDRESS, null) ?: return null
        val port = preferences().getInt(SERVER_PORT, 0)
        val secret = preferences().getString(SHARED_SECRET, null)?.toByteArray() ?: return null

        return AppVpnConfig(server, port, secret, getDisallowedPackages())
    }

    fun Context.saveAppConfig(config: ConfigFromServer) {
        preferences().edit()
            .putString(SERVER_ADDRESS, config.address)
            .putString(SHARED_SECRET, config.secret)
            .putInt(SERVER_PORT, config.port)
            .apply()
    }

    fun Context.clearAppConfig() {
        preferences().edit()
            .putString(SERVER_ADDRESS, null)
            .putString(SHARED_SECRET, null)
            .putInt(SERVER_PORT, 0)
            .apply()
    }

    fun Context.saveUser(userName: String) {
        preferences().edit()
            .putString(USERID, userName)
            .apply()
    }

    fun Context.getUser(): String? {
        return preferences().getString(USERID, null)
    }
}