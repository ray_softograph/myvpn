package com.softograph.vpnapp.data

class AddressWithPrefixLength(val address: String, val prefixLength: Int)

class ServerConfig(
    val mtu: Int,
    val addresses: List<AddressWithPrefixLength>,
    val routes: List<AddressWithPrefixLength>,
    val dnsServers: List<String>,
    val searchDomains: List<String>
)

class AppVpnConfig(
    val serverAddress: String,
    val serverPort: Int,
    val serverSecret: ByteArray,
    val disallowedPackages: List<String>
)

class VpnConfig(val server: ServerConfig, val app: AppVpnConfig)

class ConfigFromServer(
    val address: String,
    val port: Int,
    val secret: String,
    val message: String
)

class ConnectivityStats(
    var totalTx: Long = 0L,
    var totalRx: Long = 0L,
    var lastTx: Long = 0L,
    var lastRx: Long = 0L
)
